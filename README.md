# Description
  
Takes as argument an Executable and Linking Formatted (ELF) binary and  
parses the ELF sections.  
  
# Usage
  
```
chmod +x elfsct
elfsct <binary>
```

