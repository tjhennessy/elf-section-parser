#ifndef _ELF32_COMMON_H
#define _ELF32_COMMON_H

#include <stdint.h>
#include <file_handler.h>

/* General purpose macros */
#define TRUE    1
#define FALSE   0
#define SIXTEEN_BITS    2 /* in bytes */
#define THIRTYTWO_BITS  4 /* in bytes */
#define SIXTYFOUR_BITS  8 /* in bytes */

/* ELF Identification */
/* These are the initial bytes of the file and specify how to interpret
   the file.  This enables an object file framework supporting multiple
   processors, multiple data encodings, and multiple classes of machines. */
#define EI_NIDENT   16 /* Size of e_ident[] */
#define EI_MAG0     0 /* File identification */
#define EI_MAG1     1 /* File identification */
#define EI_MAG2     2 /* File identification */
#define EI_MAG3     3 /* File identification */
#define EI_CLASS    4 /* File class */
#define EI_DATA     5 /* Data encoding */
#define EI_VERSION  6 /* File version */
#define PAD         7 /* Start of padding bytes */

/* Files first four bytes are the Magic Number which identify file as
   ELF object file */
#define ELFMAG0     0x7f /* e_ident[EI_MAG0] */
#define ELFMAG1     'E'  /* e_ident[EI_MAG1] */
#define ELFMAG2     'L'  /* e_ident[EI_MAG2] */
#define ELFMAG3     'F'  /* e_ident[EI_MAG3] */

/* Identifes files class or capacity */
#define ELFCLASSNONE    0 /* Invalid class */
#define ELFCLASS32      1 /* 32-bit objects */
#define ELFCLASS64      2 /* 64-bit objects */

/* Files data encoding, endianness */
#define ELFDATANONE     0 /* Invalid data encoding */
#define ELFDATA2LSB     1 /* 2's complement little endian */
#define ELFDATA2MSB     2 /* 2's complement big endian */

/* Object file types */
#define ET_NONE     0 /* No file */
#define ET_REL      1 /* Relocatable file */
#define ET_EXEC     2 /* Executable file */
#define ET_DYN      3 /* Shared Object file */
#define ET_CORE     4 /* Core file */
#define ET_LOPROC   0xff00 /* Processor-specific */
#define ET_HIPROC   0xffff /* Processor-specific */

/* Object file architectures */
#define EM_NONE     0 /* No machine architecture */
#define EM_M32      1 /* AT&T WE 32100 */
#define EM_SPARC    2 /* SPARC */
#define EM_386      3 /* Intel 80386 */
#define EM_68K      4 /* Motorola 68000 */
#define EM_88K      5 /* Motorola 88000 */
#define EM_860      7 /* Intel 80860 */
#define EM_MIPS     8 /* MIPS RS3000 */

/* Object file versions */
#define EV_NONE     0 /* Invalid */
#define EV_CURRENT  1 /* Current */

/* Special Section Indexes */
#define SHN_UNDEF       0
#define SHN_LORESERVE   0xff00
#define SHN_LOPROC      0xff00
#define SHN_HIPROC      0xff1f
#define SHN_ABS         0xfff1
#define SHN_COMMON      0xfff2
#define SHN_HIRESERVE   0xffff

/* Section types sh_type */
#define SHT_NULL        0
#define SHT_PROGBITS    1
#define SHT_SYMTAB      2
#define SHT_STRTAB      3
#define SHT_RELA        4
#define SHT_HASH        5
#define SHT_DYNAMIC     6
#define SHT_NOTE        7
#define SHT_NOBITS      8
#define SHT_REL         9
#define SHT_SHLIB       10
#define SHT_DYNSYM      11
#define SHT_LOPROC      0x70000000
#define SHT_HIPROC      0x7fffffff
#define SHT_LOUSER      0x80000000
#define SHT_HIUSER      0xffffffff

/* Section Attribute Flags, sh_flags */
#define SHF_WRITE       0x1         /* Contains writable data during execution */
#define SHF_ALLOC       0x2         /* Occupies memory during execution */
#define SHF_EXECINSTR   0x4         /* Contains executable machine instructions */
#define SHF_MASKPROC    0xf0000000  /* Processor-specific semantic bits */

#define eprintf(FMT, ...) \
    fprintf(stderr, "%s:%d: ", __FILE__, __LINE__); \
    fprintf(stderr, FMT, __VA_ARGS__)

typedef uint32_t Elf32_Addr;  /* Unsigned program address */
typedef uint16_t Elf32_Half;  /* Unsigned meidum integer */
typedef uint32_t Elf32_Off;   /* Unsigned file offset */
typedef int Elf32_Sword;      /* Signged large integer */
typedef uint32_t Elf32_Word;  /* Usigned large integer */

typedef struct
{
    unsigned char e_ident[EI_NIDENT]; /* TODO: add comment */
    Elf32_Half e_type;               /* Identifies type of object file */
    Elf32_Half e_machine;            /* Required architecture */
    Elf32_Word e_version;            /* Identifies object file version */
    Elf32_Addr e_entry;              /* Virtual address starting point */
    Elf32_Off  e_phoff;              /* Program header offset in bytes */
    Elf32_Off  e_shoff;              /* Section header offset in bytes */
    Elf32_Word e_flags;              /* Processor specific flags */
    Elf32_Half e_ehsize;             /* Elf header size in bytes */
    Elf32_Half e_phentsize;          /* Program header entry size */
    Elf32_Half e_phnum;              /* Number of entries in program header */
    Elf32_Half e_shentsize;          /* Section header size in bytes */
    Elf32_Half e_shnum;              /* Number of entries in section header */
    Elf32_Half e_shstrndx;           /* Section header table index of name */
} Elf32_Ehdr;

typedef struct
{
    Elf32_Word      sh_name;        /* Index into header string table */
    Elf32_Word      sh_type;        /* Identifies contents and semantics */
    Elf32_Word      sh_flags;       /* 1-bit flags describing attributes */
    Elf32_Addr      sh_addr;        /* Gives first bytes address */
    Elf32_Off       sh_offset;      /* Offset from file to section */
    Elf32_Word      sh_size;        /* Size of section in bytes */
    Elf32_Word      sh_link;        /* Link to section header table */
    Elf32_Word      sh_info;        /* Extra info dependent on type */
    Elf32_Word      sh_addralign;   /* Section address alignment constraints */
    Elf32_Word      sh_entsize;     /* Size in bytes if fixed size */
} Elf32_Shdr;

/**
 * TODO: add comments
 */
void free_shdr(Elf32_Shdr **shdr);
/**
 * TODO: add comments
 */
void free_ehdr(Elf32_Ehdr **ehdr);

/**
 * TODO: add comments
 */
void free_buffer(file_buffer_t *buffer);

/**
 * @brief Ensures the destination bytes are formatted according to 
 * to the host architecture.
 * 
 * @param dest Stores bytes according to host byte order.
 * @param src Bytes which are copied and converted into host byte order.
 * @param byte_order Determines host byte order.
 */
void handle_endian16(uint16_t *dest, uint16_t src, uint8_t byte_order);

/**
 * @brief Ensures the destination bytes are formatted according to 
 * to the host architecture.
 * 
 * @param dest Stores bytes according to host byte order.
 * @param src Bytes which are copied and converted into host byte order.
 * @param byte_order Determines host byte order.
 */
void handle_endian32(uint32_t *dest, uint32_t src, uint8_t byte_order);

/**
 * @brief Ensures the destination bytes are formatted according to 
 * to the host architecture.
 * 
 * @param dest Stores bytes according to host byte order.
 * @param src Bytes which are copied and converted into host byte order.
 * @param byte_order Determines host byte order.
 */
void handle_endian64(uint64_t *dest, uint64_t src, uint8_t byte_order);

#endif /* _ELF32_COMMON_H */