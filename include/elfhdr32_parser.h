#ifndef _ELFHDR_PARSER_
#define _ELFHDR_PARSER_

#include <stdint.h>
#include <file_handler.h>
#include <elf32_common.h>

// TODO: add doxygen comments
Elf32_Ehdr* parse_elfhdr32(file_buffer_t buffer);
// TODO: add doxygen comments
void print_elfhdr32(Elf32_Ehdr *hdr);

#endif /* _ELFHDR_PARSER_ */
