#ifndef _ELFSCT32_PARSER_H
#define _ELFSCT32_PARSER_H

#include <elf32_common.h>

typedef struct
{
    Elf32_Off offset; /* byte offset in file of section header table */
    Elf32_Half num;   /* total number of sections */
    Elf32_Half size;  /* size in bytes of each section */
} elf_section_info_t;

typedef struct 
{
    elf_section_info_t info;
    Elf32_Shdr *shdr;
} elf_sections_t;

/**
 * TODO: comments
 */
elf_sections_t *parse_elfsct32(char *filename);

/**
 * @brief Takes a raw binary file corresponding to a 32-bit ELF binary
 * and returns an elf_section_info_t structure to help parse all
 * program section headers.
 * 
 * @param buffer Is a file_buffer_t which contains ELF 32-bit binary bytes.
 * @return On success an elf_section_t, aborts program on failure.
 */
elf_section_info_t get_section_info(file_buffer_t buffer);

#endif /* _ELFSCT32_PARSER_H */