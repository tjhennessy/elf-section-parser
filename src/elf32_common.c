#include <elf32_common.h>
#include <file_handler.h>

/**
 * TODO: add comments
 */
void free_shdr(Elf32_Shdr **shdr)
{
    if (*shdr != NULL)
    {
#ifndef NDEBUG
    printf("%s\n", "Freeing section header");
#endif
        free(*shdr);
        *shdr = NULL;
    }
}

/**
 * TODO: add comments
 */
void free_ehdr(Elf32_Ehdr **ehdr)
{
    if (*ehdr != NULL)
    {
#ifndef NDEBUG
    printf("%s\n", "Freeing ELF header");
#endif
        free(*ehdr);
        *ehdr = NULL;
    }
}

/**
 * TODO: add comments
 */
void free_buffer(file_buffer_t *buffer)
{
    if (*buffer != NULL)
    {
#ifndef NDEBUG
    printf("%s\n", "Freeing buffer");
    printf("going to free %p and then %p\n", &(*buffer)->bytes, *buffer);
#endif
        free((*buffer)->bytes);
        free(*buffer);
        *buffer = NULL;
    }
}

/**
 * @brief Ensures the destination bytes are formatted according to 
 * to the host architecture.
 * 
 * @param dest Stores bytes according to host byte order.
 * @param src Bytes which are copied and converted into host byte order.
 * @param byte_order Determines host byte order.
 */
void handle_endian16(uint16_t *dest, uint16_t src, uint8_t byte_order)
{
    switch (byte_order)
    {   // to little endian
        case ELFDATA2LSB:
            *dest = htole16(src);
        break;
        // to big endian
        case ELFDATA2MSB:
            *dest = htobe16(src);
        break;
        default:
        break;
    }
}

/**
 * @brief Ensures the destination bytes are formatted according to 
 * to the host architecture.
 * 
 * @param dest Stores bytes according to host byte order.
 * @param src Bytes which are copied and converted into host byte order.
 * @param byte_order Determines host byte order.
 */
void handle_endian32(uint32_t *dest, uint32_t src, uint8_t byte_order)
{
    switch (byte_order)
    {   // to little endian
        case ELFDATA2LSB:
            *dest = htole32(src);
        break;
        // to big endian
        case ELFDATA2MSB:
            *dest = htobe32(src);
        break;
        default:
        break;
    }
}

/**
 * @brief Ensures the destination bytes are formatted according to 
 * to the host architecture.
 * 
 * @param dest Stores bytes according to host byte order.
 * @param src Bytes which are copied and converted into host byte order.
 * @param byte_order Determines host byte order.
 */
void handle_endian64(uint64_t *dest, uint64_t src, uint8_t byte_order)
{
    switch (byte_order)
    {   // to little endian
        case ELFDATA2LSB:
            *dest = htole64(src);
        break;
        // to big endian
        case ELFDATA2MSB:
            *dest = htobe64(src);
        break;
        default:
        break;
    }
}