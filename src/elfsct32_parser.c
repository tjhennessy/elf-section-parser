#include <stdio.h>
#include <elfsct32_parser.h>
#include <elfhdr32_parser.h>
#include <elf32_common.h>
#include <file_handler.h>

static void failure_abort(char *error)
{
    fprintf(stderr, "Error: %s\n", error);
    exit(EXIT_FAILURE);
}

static void free_section_info(elf_section_info_t **section_info)
{
    if (*section_info != NULL)
    {
#ifndef NDEBUG
    printf("Freeing section info\n");
#endif
        free(*section_info);
        *section_info = NULL;
    }
}

static void free_sections(elf_sections_t **sections)
{
    if (*sections != NULL)
    {
#ifndef NDEBUG
    printf("Freeing sections\n");
#endif
        free((*sections)->shdr);
        free(*sections);
        *sections = NULL;
    }
}

elf_sections_t *parse_elfsct32(char *filename)
{
    file_buffer_t buffer = NULL;
    elf_sections_t *sections = NULL;
    uint32_t current_offset;
    int i;

    if (NULL == filename)
    {
        failure_abort("passed in NULL string");
    }

    /* Reads the binary into a buffer in raw format */
    buffer = get_file_buffer(filename);
    if (NULL == buffer)
    {
        failure_abort("could not buffer file");
    }

    sections = calloc(1, sizeof(elf_sections_t));
    if (NULL == sections)
    {
        failure_abort("memory allocation failed");
    }

    /* Retrieves offset into buffer of section headers, the number
       of section headers as well as the size of each */
    sections->info = get_section_info(buffer);

#ifndef NDEBUG
    printf("Number of sections is %hu and section size is %hu\n",
        sections->info.num, sections->info.size);
#endif 

    /* Ensure our section header storage structure has enough space to
       store all of the section headers */
    sections->shdr = calloc(sections->info.num, sections->info.size);
    if (NULL == sections->shdr)
    {
        failure_abort("memory allocation failed");
    }

    /* Copies all section headers from buffer into our storage structure */
    memcpy(sections->shdr, buffer->bytes + sections->info.offset, 
        sections->info.num * sections->info.size);

    for (i = 0; i < sections->info.num; i++)
    {
        // current_offset = sections->info.offset + sections->info.size * i;
        /* I would like to easily print out each string name of the section; however,
            because I do not maintain the elf header I lost the e_shstrndx member which
            is necessary because it is the index  of the section which holds the string
            table.  For now, I will just print out a part from each section header to
            validate that the memcpy above worked to successfully transfer the bytes
            from the buffer into our section header data structure. 
        */
       printf("%d size is %u, ", i, sections->shdr[i].sh_size);
       printf("entry size is %u\n", sections->shdr[i].sh_entsize);
    }

    free(buffer->bytes);
    free(buffer);

    return sections;
}


elf_section_info_t get_section_info(file_buffer_t buffer)
{
    Elf32_Ehdr __attribute__ ((cleanup(free_ehdr))) *ehdr = NULL; 
    elf_section_info_t section_info;

    ehdr = parse_elfhdr32(buffer);

    /* All members already in host byte order */
    section_info.offset = ehdr->e_shoff;
    section_info.size = ehdr->e_shentsize;
    section_info.num = ehdr->e_shnum;

#ifndef NDEBUG
    printf("Section header offset is %u\n", section_info.offset);
    printf("Section header sizes are %u bytes\n", section_info.size);
    printf("Section header contains %u headers\n", section_info.num);
#endif

    return section_info;  
}

int main(int argc, char *argv[])
{
    elf_sections_t __attribute__ ((cleanup(free_sections))) *sections = NULL;

    if (argc < 2)
    {
        fprintf(stderr, "Usage: %s <file name>\n", argv[0]);
        return 1;
    }

    sections = parse_elfsct32(argv[1]);

    printf("here\n");

    // print out sections
    
    return 0;
}