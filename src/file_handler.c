#include <file_handler.h>

/**
 * @brief Private function for use with gcc cleanup attribute.
 */
static void close_file(FILE **fd)
{
    if (*fd != NULL)
    {
#ifndef NDEBUG
    printf("%s\n", "Closing file");
#endif
        fclose(*fd);
    }
}

file_size_t get_file_size(FILE *fp)
{
    long size;

    fseek(fp, 0L, SEEK_END);
    size = ftell(fp);
    rewind(fp);

#ifndef NDEBUG
    printf("size of file is %ld\n", size);
#endif

    return size;
}

file_buffer_t get_file_buffer(char *filename)
{
    size_t num_bytes;
    file_buffer_t buffer = NULL;
    FILE __attribute__((cleanup(close_file))) *ifp = NULL;

#ifndef NDEBUG
    printf("%s%s\n", "parsing elf file -> ", filename);
#endif 

    ifp = fopen(filename, "rb");
    if (NULL == ifp)
    {
        fprintf(stderr, "Error: %s\n", strerror(errno));
        return NULL;
    }

    buffer = calloc(1, sizeof(file_buffer));
    if (NULL == buffer)
    {
        fprintf(stderr, "Error: %s\n", "could not allocate memory");
        exit(EXIT_FAILURE);
    }


    buffer->size = get_file_size(ifp);
    if (buffer->size < 0)
    {
        fprintf(stderr, "Error: could not determine file size: %s\n", strerror(errno));
        return NULL;
    }

#ifndef NDEBUG
    printf("Allocated memory for file buffer type\n");
#endif

    buffer->bytes = calloc(buffer->size, sizeof(uint8_t));
    if (NULL == buffer->bytes)
    {
        fprintf(stderr, "Error: %s\n", "could not allocate memory");
        exit(EXIT_FAILURE);
    }

    num_bytes = fread(buffer->bytes, 1, buffer->size, ifp);
    if (num_bytes != buffer->size)
    {
        fprintf(stderr, "Error: %s\n", "could not read file into buffer");
        return NULL;
    }

#ifndef NDEBUG
    printf("File read into file buffer structure\n");
    print_file_bytes(buffer);
#endif

    return buffer;
}

void print_file_bytes(file_buffer_t buff)
{
    int i;
    int j;
    int k;

#ifndef NDEBUG
    printf("Printing out file bytes\n");
#endif

    for (i = 0; i < buff->size; i++)
    {
        printf("%02X ", buff->bytes[i]);

        if (i % 16 == 0 && i > 0)
        {
            printf("\n");
        }
    }
}